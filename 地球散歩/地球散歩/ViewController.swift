//
//  ViewController.swift
//  地球散歩
//
//  Created by 岩竹弘樹 on 2020/09/06.
//  Copyright © 2020 岩竹弘樹. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // SCNViewを構築してUIViewController のビューに設定する
        let scnView = SCNView(frame: self.view.frame)
        scnView.backgroundColor = UIColor.black // 背景を黒色に
        scnView.allowsCameraControl = true // ユーザーによる視点操作を可能に
        scnView.showsStatistics = false // 描画パフォーマンス情報を表示
        self.view = scnView

        // SCNScene を SCNView に設定する
        let scene = SCNScene()
        scnView.scene = scene

        // カメラをシーンに追加する
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 15)
        scene.rootNode.addChildNode(cameraNode)

        // 無指向性の光源をシーンに追加する
        let omniLight = SCNLight()
        omniLight.type = .omni
        let omniLightNode = SCNNode()
        omniLightNode.light = omniLight
        omniLightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        scene.rootNode.addChildNode(omniLightNode)

        // あらゆる方向から照らす光源をシーンに追加する
        let ambientLight = SCNLight()
        ambientLight.type = .ambient
        ambientLight.color = UIColor.blue
        let ambientLightNode = SCNNode()
        ambientLightNode.light = ambientLight
        scene.rootNode.addChildNode(ambientLightNode)
        
        // 球体をシーンに追加する
        let sphere:SCNGeometry = SCNSphere(radius: 1.5)
        let sphereNode = SCNNode(geometry: sphere)
        sphereNode.position = SCNVector3(x: 0, y: 3, z: 0)
        scene.rootNode.addChildNode(sphereNode)
    }


}

